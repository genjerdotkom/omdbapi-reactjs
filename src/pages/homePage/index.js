import React,{ useEffect, useState } from 'react';
import { HomeTemplate } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import { getMovies } from '../../modules/redux/actions/moviesActions';
import { useLocation } from 'react-router-dom';

const HomePage = () => {
    const { loading, moviesList } = useSelector(state => state.moviesReducer);
    const [page, setPage] = useState(1);
    const dispatch = useDispatch();
    const { search } = useLocation();
    const searchParams = new URLSearchParams(search);
    const keyword = searchParams.get('keyword') || 'batman';
    const totalPage = moviesList.page.totalPage;

    useEffect(() => {
        const handleScroll = () => {
            if ((window.innerHeight + Math.ceil(window.pageYOffset) * 1.5) >= document.documentElement.offsetHeight) {
                if(!loading && page <= totalPage){
                    setPage(page + 1)
                }
            }
        }
        window.addEventListener("scroll", handleScroll);
        return () => window.removeEventListener("scroll", handleScroll);
    }, [page, totalPage, loading]);

    useEffect(() => {
        dispatch(getMovies(keyword, page));
    }, [keyword, dispatch, page])

    return (
        <HomeTemplate />
    )
}

export default HomePage
