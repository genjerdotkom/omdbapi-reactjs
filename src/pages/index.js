import HomePage from "./homePage";
import MovieDetailPage from "./movieDetailPage";
import { error404, error500 } from "./errors";

export { 
    HomePage, 
    MovieDetailPage,
    error404,
    error500 
};
