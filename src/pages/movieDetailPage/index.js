import React, {useEffect} from 'react';
import { MovieDetailTemplate } from '../../components';
import { withRouter } from 'react-router-dom';
import { getMovieDetail } from '../../modules/redux/actions/movieDetailActions';
import { useDispatch } from 'react-redux';

const MovieDetailPage = (props) => {
    const dispatch = useDispatch();

    useEffect(() => {
        const id = props.match.params.id;
        window.scrollTo(0, 0);
        dispatch(getMovieDetail(id));  
    }, [dispatch, props])

    return (
        <MovieDetailTemplate />
    )
}

export default withRouter(MovieDetailPage)
