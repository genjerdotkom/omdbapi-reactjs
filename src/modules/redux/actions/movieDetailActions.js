
import { DetailMovie } from '../../api/axios';
import { 
    GET_MOVIES_DETAIL,
    REQUEST_MOVIES_DETAIL,
    RESET_MOVIES
} from '../types';

const getMovieDetail = (id) => async (dispatch) => {
    try{
        let results = []
        
        await dispatch({ type: REQUEST_MOVIES_DETAIL })
        await dispatch({ type: RESET_MOVIES})
        await new Promise(resolve => setTimeout(resolve, 2000))

        const response = await DetailMovie(id);

        if(response.data.Response === "True"){
            results = response.data;
        }

        await dispatch({
            type: GET_MOVIES_DETAIL,
            error: false,
            loading: false,
            payload: {
                data: results
            }
        })

    }catch(error) {
        console.log(error)
    }
}

export {
    getMovieDetail
}