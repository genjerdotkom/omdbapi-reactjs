
import { SearchMovies } from '../../api/axios';
import { 
    GET_MOVIES_SUCCESS,
    REQUEST_MOVIES,
    GET_MOVIES_ERROR,
    SET_KEYWORD_MOVIES
} from '../types';

const setKeyword = (keyword) => (dispatch) => {
    dispatch({
        type: SET_KEYWORD_MOVIES, 
        keyword: keyword
    })
}

const getMovies = (keyword, page) => async (dispatch) => {
    try{

        let totalResults,
            totalPage = 1,
            results = [],
            response = {}

        await dispatch({ type: REQUEST_MOVIES })
        await new Promise(resolve => setTimeout(resolve, 2000))

        response = await SearchMovies(keyword, page);

        if(response.data.Response === "True"){
            totalResults = response.data.totalResults;
            totalPage = Math.ceil(response.data.totalResults / response.data.Search.length) ;
            results = response.data.Search;
        }
        
        await dispatch({
            type: GET_MOVIES_SUCCESS,
            error: false,
            keyword,
            loading: false,
            payload: {
                data: results,
                page: {
                    currentPage: page, 
                    totalPage: totalPage
                },
                totalResults: totalResults
            }
        })

    }catch(error) {
        console.log(error)
        await dispatch({
            type: GET_MOVIES_ERROR,
            error: true,
            keyword
        })
    }
}

export {
    getMovies,
    setKeyword
}