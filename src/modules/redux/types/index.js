module.exports = {
    GET_MOVIES_SUCCESS: 'GET_MOVIES_SUCCESS',
    GET_MOVIES_ERROR: 'GET_MOVIES_ERROR',
    REQUEST_MOVIES: 'REQUEST_MOVIES',
    SET_KEYWORD_MOVIES: 'SET_KEYWORD_MOVIES',
    GET_MOVIES_DETAIL: 'GET_MOVIES_DETAIL',
    REQUEST_MOVIES_DETAIL: 'REQUEST_MOVIES_DETAIL',
    RESET_MOVIES: 'RESET_MOVIES'
};