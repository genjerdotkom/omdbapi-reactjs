import { 
    GET_MOVIES_DETAIL,
    REQUEST_MOVIES_DETAIL
} from '../types';

const initialState = {
    loading: true,
    error: false,
    movieDetail: {}
}

const movieDetailReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MOVIES_DETAIL:
            return {
                ...state,
                loading: false,
                error: false,
                movieDetail: action.payload.data
            }
        case REQUEST_MOVIES_DETAIL:
            return {
                ...state,
                loading: true,
                error: false
            }
        default:
            return state;
    }
}

export default movieDetailReducer