import { 
    GET_MOVIES_SUCCESS,
    GET_MOVIES_ERROR,
    REQUEST_MOVIES,
    SET_KEYWORD_MOVIES,
    RESET_MOVIES
} from '../types';

const initialState = {
    loading: true,
    error: false,
    keyword: '',
    moviesList: {
        data: [],
        page: {
            currentPage: 1,
            totalPage: 1
        },
        totalResults: 0
    }
}

const moviesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MOVIES_SUCCESS:
            return {
                ...state,
                loading: false,
                keyword: action.keyword,
                error: false,
                moviesList: {
                    data: state.moviesList.data.concat(action.payload.data),
                    page: action.payload.page,
                    totalResults: action.payload.totalResults ? action.payload.totalResults : state.moviesList.totalResults
                }
            }
        case GET_MOVIES_ERROR:
            return {
                ...state,
                loading: false,
                keyword: action.keyword,
                error: true
            }
        case REQUEST_MOVIES:
            return {
                ...state,
                loading: true,
                error: false
            }
        case SET_KEYWORD_MOVIES:
            return {
                ...state,
                loading: true,
                error: false,
                keyword: action.keyword,
                moviesList: {
                    data: [],
                    page: {
                        currentPage: 1,
                        totalPage: 0
                    },
                    totalResults:0
                }
            }
        case RESET_MOVIES: {
            return initialState;
            }
        default:
            return state;
    }
}

export default moviesReducer