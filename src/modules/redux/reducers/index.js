import {combineReducers} from 'redux';
import globalReducer from './globalReducer';
import moviesReducer from './moviesReducer';
import movieDetailReducer from './movieDetailReducer';

const reducer = combineReducers({
    globalReducer,
    moviesReducer,
    movieDetailReducer
});

export default reducer;