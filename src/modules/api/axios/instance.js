const axios = require('axios');

const instance = axios.create({
    baseURL: 'https://www.omdbapi.com',
    timeOut: 1000
});

export default instance