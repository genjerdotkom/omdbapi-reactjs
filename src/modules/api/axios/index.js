import { SearchMovies, DetailMovie } from "./movies";

export {
    SearchMovies,
    DetailMovie
}