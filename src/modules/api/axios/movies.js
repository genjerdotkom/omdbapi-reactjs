import instance from "./instance";

const endpoint = (payload = {}) => {
    return {
        SEARCH_MOVIE:`/?apikey=df6558f3&s=${payload.keyword}&page=${payload.page}`,
        DETAIL_MOVIE:`/?apikey=df6558f3&i=${payload.id}`
    }
}

const SearchMovies = async (keyword, page) => {
    try{
        return await instance.get(endpoint({keyword, page}).SEARCH_MOVIE)
    }catch(error){
        console.log('error Search Movies',error)
    }
}

const DetailMovie = async (id) => {
    try{
        return await instance.get(endpoint({id}).DETAIL_MOVIE)
    }catch(error){
        console.log('error Search Movies',error)
    }
}

export {
    SearchMovies,
    DetailMovie
}