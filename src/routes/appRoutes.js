import { HomePage, MovieDetailPage } from "../pages";

const appRoutes = [ 
    {
        name: "MovieDetailPage",
        component: MovieDetailPage,
        key:"movieDetailPage",
        path: "/movie/:id"
    },
    {
        name: "Homepage",
        component: HomePage,
        exact: true,
        key:"homepage",
        path: "/"
    }
];

export default appRoutes;
