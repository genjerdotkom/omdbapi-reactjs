import React from 'react';
import { SearchBar } from '../..';
import { Header, Footer, Movies } from '../../organisms';
import "./style.scss";

const HomeTemplate = () => {
    return (
        <React.Fragment>
            <Header />
                <main className="home-wrapper">
                    <SearchBar/>
                    <Movies />
                </main>
            <Footer />
        </React.Fragment>
    )
}

export default HomeTemplate
