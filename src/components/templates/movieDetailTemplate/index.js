import React from 'react';
import { Header, Footer, MovieDetail } from '../../organisms';
import "./style.scss";

const MovieDetailTemplate = () => {
    return (
        <React.Fragment>
            <Header />
                <main className="home-wrapper">
                    <MovieDetail />
                </main>
            <Footer />
        </React.Fragment>
    )
}

export default MovieDetailTemplate
