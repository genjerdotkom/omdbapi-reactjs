import React from 'react';
import { Text, Heading, Anchor } from '../../atoms';

const MovieDescription = ({...props}) => {
    const {
        Title,
        Genre,
        Actors,
        Director,
        Runtime,
        Writer,
        Year
    } = props
    return (
        <React.Fragment>
            <Heading className="cursor-pointer text-left bold" tag="h3" visualLevel="5">
                {Title}
            </Heading>
            <Text className="text-movie--item text-left detail-list" size="s">
                Genre: <Anchor title={Genre} className="link" size="s"  />
            </Text> 
            <Text className="text-movie--item text-left detail-list" size="s">
                Actors: <Anchor title={Actors} className="link" size="s"  />
            </Text>  
            <Text className="text-movie--item text-left detail-list" size="s">
                Director: <Anchor title={Director} className="link" size="s"  />
            </Text> 
            <Text className="text-movie--item text-left detail-list" size="s">
                Runtime: <Anchor title={Runtime} className="link" size="s"  />
            </Text>  
            <Text className="text-movie--item text-left detail-list" size="s">
                Writer: <Anchor title={Writer} className="link" size="s"  />
            </Text>  
            <Text className="text-movie--item text-left detail-list" size="s">
                Year: <Anchor title={Year} className="link" size="s"  />
            </Text> 
        </React.Fragment>
    )
}

export default MovieDescription
