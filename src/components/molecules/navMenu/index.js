import React from 'react'
import { Anchor } from '../../atoms';
import { useHistory } from 'react-router';
import "./style.scss";

const NavMenu = ({...props}) => {
    const history = useHistory(); 

    return (
        <ul className="nav">
            <li className="nav--item">
                <Anchor className="link" onClick={() => {history.push(`/`)}} title="Home" {...props} />
            </li>
        </ul>
    )
}

export default NavMenu
