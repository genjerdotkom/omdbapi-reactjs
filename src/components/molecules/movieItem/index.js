import React from 'react';
import { Text, Anchor, Heading, Images } from '../../atoms';
import { useHistory } from 'react-router-dom';
import { IMGTextYellow } from '../../../assets';
import { Modal, useModal } from '..';

import "./style.scss";

const NewsItem = (props) => {
    const history = useHistory();
    const { isShowing, toggle } = useModal(false);
    
    const handleDetail = () => {
        history.push(`/movie/${props.imdbID}`)
    }

    return (
        <li className="movies--item">
            <div className="movies--item--cover">
                <Images 
                    onClick={toggle} 
                    onError={(e)=>{e.target.onerror = null; e.target.src=IMGTextYellow}} 
                    className="img-responsive" 
                    src={props.Poster}
                />
            </div>
            <div className="wrapper--item--text">
                <Heading className="cursor-pointer text-left" tag="h3" visualLevel="5" onClick={handleDetail}>
                    <Anchor title={props.Title} className="link" size="m"  />
                </Heading>
                <Text className="text-movie--item text-left" size="s">
                    Years {props.Year}, 
                    Type {props.Type}
                </Text> 
            </div>

            <Modal 
                isShowing={isShowing}
                hide={toggle}
                children={<Images 
                    onError={(e)=>{e.target.onerror = null; e.target.src=IMGTextYellow}} 
                    className="img-responsive" 
                    src={props.Poster}
                />}
            />
            
        </li>
    )
}

export default NewsItem
