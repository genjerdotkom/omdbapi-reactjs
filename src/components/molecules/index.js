import NavMenu from "./navMenu";
import SocialMedia from "./socialMedia";
import MovieItem from "./movieItem";
import SearchBar from "./searchBar";
import {Modal, useModal } from "./modal";
import MovieDescription from "./movieDescription";
import MovieCover from "./movieCover";

export {
    NavMenu,
    SocialMedia,
    MovieItem,
    SearchBar,
    Modal,
    useModal,
    MovieDescription,
    MovieCover
};