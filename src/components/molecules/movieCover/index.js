import React from 'react';
import { Images } from '../../atoms';
import { IMGTextYellow } from '../../../assets';

import "./style.scss";

const MovieCover = ({...props}) => {
    return (
        <div className="movies--item--cover">
            <Images 
                className={[props.className].join(" ")} 
                onError={(e)=>{e.target.onerror = null; e.target.src=IMGTextYellow}} 
                src={props.cover}
                alt={props.title}
            />
        </div>
    )
}

export default MovieCover
