import Header from "./header"
import Footer from "./footer"
import Movies from "./movies"
import MovieDetail from "./movieDetail"

export {
    Header,
    Footer,
    Movies,
    MovieDetail
}