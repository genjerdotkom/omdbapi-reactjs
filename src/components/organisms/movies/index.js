import React from 'react';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import { MovieItem } from '../../molecules';
import { Text, Heading } from '../../atoms';
import { useSelector } from 'react-redux';
import { ShortNumberFormat } from '../../../utils/numberFormat/shortFormat';

import "./style.scss";
import 'react-loading-skeleton/dist/skeleton.css'

const Movies = () => {
    const { moviesList, loading, error, keyword } = useSelector(state => state.moviesReducer);

    const loader = (count) => {
        let skleton = [];
        for (let i = 0; i < count; i++) {
            skleton.push(
                <li className="movies--items" key={i} >
                    <SkeletonTheme baseColor="#202020" highlightColor="#444">
                        <Skeleton height={280}/>
                        <Skeleton width={`100%`} height={45} style={{marginTop:"10px"}}/>
                        <Skeleton width={`100%`} height={45} style={{marginTop:"10px"}}/>
                    </SkeletonTheme>
                </li>
            );
        }
        return skleton
    }

    return (
        <div className="moviesContainer">
            {
                keyword !== '' 
                ? <Heading style={{marginBottom:25}} tag="h4" visualLevel="4" className="color-yellow bold">
                    {`Search keyword "${keyword}" total ${ShortNumberFormat(moviesList.totalResults)}`}  
                    </Heading>
                : ""
            }
            
            { 
               !loading && error 
                ?   <Text className="text-center padding-20">No Data record</Text> 
                :   <ul className="movies">
                        { 
                            moviesList.data.map((row,i) => {
                                return <MovieItem key={row.imdbID} {...row} />
                            })
                        }
                        {loading ? loader(10) : ""}
                    </ul>
            }

            {
                !loading && !error && moviesList.page.currentPage >= moviesList.page.totalPage 
                ? <Text className="text-center padding-20">No more</Text> 
                : ""
            }
        </div>
    )
}

export default Movies
