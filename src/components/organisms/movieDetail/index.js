import React from 'react';
import { MovieCover } from '../../molecules';
import { Anchor} from '../../atoms';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import { MovieDescription } from '../..';

import "./style.scss";
import 'react-loading-skeleton/dist/skeleton.css'

const MovieDetail = () => {
    const { movieDetail, loading } = useSelector(state => state.movieDetailReducer);
    const history = useHistory();

    const loaderCover = () => {
        return <SkeletonTheme baseColor="#202020" highlightColor="#444">
            <Skeleton height={280}/>
        </SkeletonTheme>
    }

    const loaderDescription = () => {
        return <SkeletonTheme baseColor="#202020" highlightColor="#444">
            <Skeleton height={40}/>
            <Skeleton style={{marginTop:15}} height={20}/>
            <Skeleton style={{marginTop:15}} height={20}/>
            <Skeleton style={{marginTop:15}} height={20}/>
            <Skeleton style={{marginTop:15}} height={20}/>
            <Skeleton style={{marginTop:15}} height={20}/>
        </SkeletonTheme>
    }

    return (
        <div className="moviesDetailContainer">
            <ul className="movies-detail">
                <li className="movies--item">
                    { loading ? loaderCover() : <MovieCover cover={movieDetail.Poster} /> }
                </li>
                <li>
                    { loading ? loaderDescription() : <MovieDescription {...movieDetail}/> }
                </li>
            </ul>
            <Anchor title="Back" className="link back-link" size="m" onClick={()=>history.push("/")} />
        </div>
    )
}

export default MovieDetail
