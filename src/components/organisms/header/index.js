import React from 'react'
import NavMenu from '../../molecules/navMenu';
import Logo from '../../atoms/logo';
import "./style.scss";

const Header = () => {
    return (
        <div className="header">
            <div className="container">
                <Logo className="logo"/>
                <NavMenu />
            </div>
        </div>
    )
}

export default Header
