import LogoImg from "./images/logo.jpg"; 
import ICLinkedin from "./icons/linkedin.png";
import ICInstagram from "./icons/instagram.png";
import ICFacebook from "./icons/facebook.png";
import IMGTextYellow from "./images/text-yellow.png";
import ICSearch from "./images/search.png";

export { 
    LogoImg,
    ICLinkedin,
    ICInstagram,
    ICFacebook,
    IMGTextYellow,
    ICSearch
}