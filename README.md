# OMDBAPI ReactJS 

[OMDBAPI](https://omdbapi.com/)

## Case Study

#### Get Movies From Search Keyword
#### Get Movie Detail

## Getting started
### Requirements

- NodeJS v12.12.0 Or higher

### Installation 

Install dependency

```console 
yarn install
```
### Run development

Run application

```console 
yarn start
```

Akses -> [http://localhost:3000](http://localhost:3000)

### Run production on local server

Build Code & Run

```console
yarn build

yarn global add serve

serve -s build
```

Akses -> [http://localhost:3000](http://localhost:3000)

### Using Docker compose

Bundle images & webserver (nginx)

```console 
docker-compose up -d
```

Akses -> [http://localhost:3000](http://localhost:3000)

## Screenshoot

![clean architecture](screenshoot.png)

## Demo

[Click](https://omdbapi-reactjs-ginanjardp.web.app/)

### Thank You 

GinanjarDP
[Linkeddin](https://id.linkedin.com/in/ginanjar-putranto-0416a913b)